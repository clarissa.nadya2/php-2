
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        $kalimat1 = "Hello PHP!";
        echo "Kalimat1 = " . $kalimat1 . "<br>";
        echo "Panjang String Kalimat 1 = " . strlen($kalimat1) ."<br>";
        echo "Jumlah kalimat 1 = ". str_word_count($kalimat1) ."<br> <br>";

        $kalimat2 = "I'm ready for the challenges";
        echo "Kalimat2 = " . $kalimat2 . "<br>";
        echo "Panjang String Kalimat 2 = " . strlen($kalimat2) ."<br>";
        echo "Jumlah kalimat 2 = ". str_word_count($kalimat2) ."<br> <br>";
       
        $kalimat3 = "I love PHP";
        echo "Kalimat ke 3 = ". $kalimat3 ."<br>";
        echo "Kata 1 kalimat ke 3 = ". substr($kalimat3,0,1). "<br>";
        echo "Kata 2 kalimat ke 3 = ". substr($kalimat3,2,4). "<br>";
        echo "Kata 3 kalimat ke 3 = ". substr($kalimat3,7,3). "<br> <br>";

        $kalimat4 = "PHP is old but sexy!";
        echo "Kalimat ke 4 =" . $kalimat4 ."<br>";
        echo "Kalimat 4 = " . str_replace("PHP is old but sexy","PHP is old but awesome",$kalimat4);
    ?>
</body>
</html>